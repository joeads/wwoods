﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskSystem : MonoBehaviour
{
    public static int taskCompletion;
    private int taskCompletionMax;
    [SerializeField] Image taskCompletionImage;
    

    public void Start()
    {
        this.taskCompletionMax = 50;
        taskCompletion = 0;
        TaskImageUpdate();
        taskCompletionImage.fillAmount = 0;
    }

    //gets value of 
    public int GetTaskCompletion()
    {
        return taskCompletion;
    }

    public float GetTaskCompletionPercent()
    {
        return (float)taskCompletion / taskCompletionMax;
    }
   
    public void FillTask(int taskFillAmount)
    {
        //taskCompletion = 0;
        taskCompletion += taskFillAmount;
        if (taskCompletion > taskCompletionMax) taskCompletion = taskCompletionMax;
        TaskImageUpdate();

    }

    private void TaskImageUpdate()
    {
        if(taskCompletion < 0 || taskCompletion == 0)
        {

            return;
            

        }

        else
        {

            taskCompletionImage.fillAmount += (float)taskCompletion / taskCompletionMax;
            

        }
        taskCompletion = 0;
        
    }

}


